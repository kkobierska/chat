class Post < ActiveRecord::Base
  belongs_to :user, counter_cache: true
  default_scope -> { order(created_at: :desc) }
  # validates :text, presence: true, length: {maximum: 140}
  # validates :title, presence: true
  # validates :user_id, presence: true
end
