class User < ActiveRecord::Base
  has_many :posts, dependent: :destroy
  has_many :friendships
  has_many :friends, through: :friendships

  has_secure_password
  # validates :username, presence: true, uniqueness: true, length: {maximum: 20}
  # validates :password, length: {minimum: 6}
  # validates :email, presence: true, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i, on: :create }

end
