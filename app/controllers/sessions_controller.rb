class SessionsController < ApplicationController
  include SessionsHelper

  def new
  end

  def show
    @user = User.find(params[:id])
  end

  def create
    user = User.find_by_username(params[:session][:username].downcase)
    if user && user.authenticate(params[:session][:password])
      log_in user
      redirect_to user
      flash[:success] = 'Logged in'
    else
      flash.now[:danger] = "Invalid email or password"
      render :new
    end
  end

  def destroy
    log_out
    redirect_to login_path
    flash[:info] = "Logged out"
  end

  private

  def user_params
    params.require(:user).permit(:username, :password)
  end
end
