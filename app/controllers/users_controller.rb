class UsersController < ApplicationController

  def index
    @users = User.all.where.not(id: current_user.id).decorate
    @friendship = current_user.friendships.find_by(:friend_id => params[:id])
  end

  def show
    @user = User.find(params[:id])
    @posts = @user.posts.paginate(page: params[:page], per_page: 5)
  end

  def new
    @user = User.new
  end

  def create 
    @user = User.new(user_params)
    if @user.save
      UserMailer.welcome_email(@user).deliver
      redirect_to login_path 
      flash[:success] = "Signed up!"
    else
      render "new"
    end
  end

  private

  def user_params
    params.require(:user).permit(:username, :email, :password, :password_confirmation)
  end
end
