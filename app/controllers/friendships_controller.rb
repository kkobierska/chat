class FriendshipsController < ApplicationController


  def create
    @friendship = current_user.friendships.build(:friend_id => params[:friend_id])
    @friend = User.find(params[:friend_id])
    @user = current_user
    
    if @friendship.save
      UserMailer.friend_added_email(@friend, @user).deliver
      redirect_to users_path
      flash[:success] = "Added friend"
    else
      redirect_to users_path
      flash[:warning] = "Already friend"
    end
  end

  def destroy
    @friendship = current_user.friendships.find_by(:friend_id => params[:id])
    # @friend = User.find_by(:friend_id)
    # @user = current_user
    
    @friendship.destroy
      # UserMailer.friend_removed_email(@friend, @user).deliver
      flash[:info] = "Removed friendship"
      redirect_to users_path
  end
end
