class PostsController < ApplicationController
  before_action :logged_in_user, only: [:create, :destroy, :update]

  def index
    @posts = Post.all
  end

  def show
    @post = Post.find(params[:id])
    @user = current_user
  end

  def new
    @post = Post.new
  end

  def create
    @user = current_user
    @post = current_user.posts.build(post_params)
    if @post.save
      redirect_to user_path(@user)
      flash[:success] = "New post added"
    else
      flash[:danger] = "Try again"
      render :new
    end
  end

  def destroy
    if @user = current_user
      @user.posts.find(params[:id]).destroy
      redirect_to @user
      flash[:success] = "Post deleted"
    else
      redirect_to user_path(@user)
      flash[:warning] = "Can't delete friend post"
    end
  end

  def edit
    @user = current_user
    @post = @user.posts.find_by(params[:user_id])
  end

  def update
    @user = current_user
    @post = @user.posts.find_by(params[:user_id])
    if @post.update_attributes(post_params)
      redirect_to user_path(@user)
      flash[:success] = "Post updated"
    else
      flash[:warning] = "Try again"
      render 'edit'
    end
  end

  private

  def post_params
    params.require(:post).permit(:title, :text)
  end
end
