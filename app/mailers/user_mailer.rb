class UserMailer < ActionMailer::Base
  default from: "from@example.com"

  def welcome_email(user)
    @user = user
    @url = 'localhost:3000'
    mail(to: @user.email, Subject: 'Welcome to My Chat')
  end

  def friend_added_email(friend, user)
    @friend = friend
    @user = user
    @url = 'localhost:3000'
    mail(to: @friend.email, Subject: 'Someone added you to Friends')
  end

  def friend_removed_email(friend, user)
    @friend = friend
    @user = user
    @url = 'localhost:3000'
    mail(to: @friend.email, Subject: 'Someone added you to Friends')
  end
end
