class AddCounterCacheToUsers < ActiveRecord::Migration
  def up
    add_column :users, :posts_count, :integer, default: 0
    User.all.each { |x| x.update!(posts_count: x.posts.count ) }
    # User.all.each do |user|
    #   user.posts_count = user.posts.length
    #   user.save!
    end

  def down
    remove_column :users, :posts_count
  end
end
